/* 
 * sphere. smooth_union, distortion SDFs from
 * from https://iquilezles.org/www/articles/distfunctions/distfunctions.htm
 */

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 pixel;

layout(set = 0, binding = 0) uniform low_frequency {
    uint width;
    uint height;
} resolution;

layout(set = 0, binding = 1) uniform high_frequency {
    float value;
} time;

layout(push_constant) uniform push_constants {
    layout(offset =  0) float blob_inc;
};

#define FLT_MAX 3.402823466e+38

const vec3 background_color = vec3(0.2);

vec3 rotate_y(vec3 p, float theta)
{
    const float c = cos(theta);
    const float s = sin(theta);

    mat4 rotation = mat4(
        c, 0, s, 0,
        0, 1, 0, 0,
       -s, 0, c, 0,
        0, 0, 0, 1
    );

    return vec3(inverse(rotation)*vec4(p, 1.0));
}

float sphere_sdf(in vec3 p, in vec3 c, float r)
{
    return length(p - c) - r;
}

float smooth_union(float d1, float d2, float k)
{
    float h = clamp(0.5 + 0.5*(d2-d1)/k, 0.0, 1.0);

    return mix(d2, d1, h) - k*h*(1.0-h);
}

// Distort a sphere_sdf at a given position and radius
float blob_sdf(vec3 p, vec3 pos, float radius)
{
    p = rotate_y(p, 1*time.value);

    float d1 = sphere_sdf(p, pos, radius);

    // Apply distortion
    float d2 = sin(4*p.x)*sin(4*p.y)*sin(4*p.z);
    d1/=1.5;
    d2/=6.0;

    return d1+d2;
}

float sdf(in vec3 p0)
{
    float distance = FLT_MAX;
    const float center_blob = blob_sdf(p0, vec3(0), 1.0);
    const float speed = 7.0;

    const float union_threshold = 0.9;

    for(float i = -5.0; i < 5.0; i += blob_inc)
    {
        const vec3 pos = vec3(5*sin(time.value) * cos(time.value/speed + i),
                              5*cos(time.value) * sin(time.value/speed + i),
                              5*sin(time.value/speed + i));

        const float pull = sin(time.value/speed);

        // generate blobs
        const vec3 pos0 = pos*pull;
        const float d0 = blob_sdf(p0, pos0, 0.25);

        // generate opposing blobs
        const vec3 pos1 = pos.yxz*pull;
        const float d1 = blob_sdf(p0, pos1, 0.25);

        // blobs can combine with the center_blob
        distance = min(distance, smooth_union(center_blob, d0, union_threshold));
        distance = min(distance, smooth_union(center_blob, d1, union_threshold));

        // smaller blobs can combine
        distance = min(distance, smooth_union(d0, d1, union_threshold));
    }

    return distance;
}

// Estimate the normal by sampling the gradient of nearyby points in varying directions
vec3 estimate_normal(vec3 p)
{
    const vec3 step = vec3(0.00001, 0.0, 0.0);

    vec3 gradient = vec3(sdf(p + step.xyy) - sdf(p - step.xyy),
                         sdf(p + step.yxy) - sdf(p - step.yxy),
                         sdf(p + step.yyx) - sdf(p - step.yyx));

    return normalize(gradient);
}

vec3 ray_march(vec3 ray_origin, vec3 ray_direction)
{
    float       depth        = 0.0;
    const uint  max_steps    = 32;
    const float hit_distance = 0.001;
    const float max_depth    = 20.0;

    for (uint i = 0; i < max_steps; ++i)
    {
        vec3 pos = ray_origin + depth * ray_direction;

        const float distance = sdf(pos);

        // Found an SDF
        if (distance < hit_distance) 
        {
            const vec3 normal = estimate_normal(pos);

            // Coloring of blobs
            return abs(vec3(0.2) - normal);
        }
        else if (depth > max_depth)
            break;

        depth += distance;
    }
    return background_color;
}

void main()
{
    const vec2 uv = vec2(gl_FragCoord.x/resolution.width -0.5,
                         gl_FragCoord.y/resolution.height-0.5);

    const vec3 view_position = vec3(0.0, 0.0, -10.0);
    const vec3 ray_direction = vec3(uv, 1.0);
    const vec3 color         = ray_march(view_position, ray_direction);

    pixel = vec4(color, 1.0);
}

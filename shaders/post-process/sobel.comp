/* Sobel operator reference
 * https://en.wikipedia.org/wiki/Sobel_operator
 *
 * 1. Calculate g_x & g_y by convolving the pixel with each kernel
 * g_x = g_x_mat*input_pixel
 * g_y = g_y_mat*input_pixel
 *
 * 2. Calculate the final g by taking the magnitude of the values
 * g = sqrt(g_x^2 + g_y^2)
 */

#version 450

#define WORKGROUP_SIZE 32

layout(local_size_x = WORKGROUP_SIZE, local_size_y = WORKGROUP_SIZE) in;

layout(set = 0, binding = 0, rgba8) readonly  uniform image2D input_image;
layout(set = 0, binding = 1, rgba8) writeonly uniform image2D output_image;

const mat3 g_x_mat = mat3(
    1.0, 0.0, -1.0,
    2.0, 0.0, -2.0,
    1.0, 0.0, -1.0
);

const mat3 g_y_mat = mat3(
    1.0,  2.0,  1.0,
    0.0,  0.0,  0.0,
   -1.0, -2.0, -1.0
);

void
main(void)
{    
    const uvec3 global = gl_GlobalInvocationID;

    vec3 output_pixel = vec3(0);

    vec2 g_sums = vec2(0.0);

    // 1. Calculate g_x & g_y by convolving the pixel with each kernel
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
        {
            const ivec2 indices = ivec2(global.x + i - 1, global.y + j - 1);

            const vec3 pixel = vec3(imageLoad(input_image, abs(indices)));

            // Obatain the grayscale value of the pixel
            const float grayscale = dot(pixel, vec3(0.3, 0.59, 0.11));

            // Use the grayscale value instead of the normal pixel color
            g_sums += grayscale*vec2(g_x_mat[i][j], g_y_mat[i][j]);
        }
    }

    // 2. Calculate the final g by taking the magnitude of the values
    const float g = length(g_sums);

    output_pixel = vec3(g);

    imageStore(output_image, ivec2(global.xy), vec4(output_pixel, 1.0));
}

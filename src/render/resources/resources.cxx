#include <ds/buffer>
#include <ds/command-pool>
#include <ds/command>
#include <ds/fence>
#include <ds/gpu>
#include <ds/image>
#include <ds/instance>
#include <ds/memory>
#include <ds/queue>
#include <ds/semaphore>
#include <ds/swap-chain>

#include "config/config.hxx"
#include "msg/msg.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

namespace dbl {

dbl::resources res;

static ds::queue_info queue_info;

ds::instance&
make_instance(void)
{
    ds::log_callback = msg_callback;

    ds::instance_info info;

    for(const char *ext : window::extensions())
        info.add_extension(ext);

    info.add_layers("VK_LAYER_KHRONOS_validation")
         .add_debug_extension();

    static ds::instance instance(info);

    return instance;
}

ds::gpu&
make_gpu(ds::instance &instance, VkSurfaceKHR surface)
{
    auto features = ds::gpu_features()
        .set_sampler_anisotropy(true);

    auto gpu_filter = ds::gpu_filter()
        .set_order        (ds::gpu_filter::order::unordered)
        .supports_surface (surface                         )
        .supports_features(features                        );

    // Graphics queue
    auto queue_filter = ds::queue_filter()
        .support_family  (ds::queue_family::graphics)
        .supports_surface(surface                    );

    std::vector<ds::gpu_candidate> gpu_candidates = instance.query_gpus(gpu_filter);

    // Take first GPU that matches filter
    if(gpu_candidates.empty())
        msg_fatal("No GPUs found");

    std::vector<ds::queue_family> graphics_families = gpu_candidates[0].query_queue_families(queue_filter);

    if(graphics_families.empty())
        msg_fatal("One of the queue queries wasn't found\n");

    auto gpu_info = ds::gpu_info()
        .set_candidate      (gpu_candidates[0]     )
        .set_features       (features              )
        .enable_queue_family(graphics_families[0].idx, 1);

    queue_info = ds::queue_info()
        .set_index       (0                       )
        .set_family_index(graphics_families[0].idx);

    static ds::gpu gpu = instance.make_gpu(gpu_info);

    return gpu;
}

static ds::buffer_info
ubo_info()
{
    return ds::buffer_info()
        .add_usage(ds::buffer_usage::uniform)
        .add_memory_usage(ds::memory_usage::host_cached   |
                          ds::memory_usage::host_coherent |
                          ds::memory_usage::host_visible  );
}

static ds::swap_chain_info
swap_chain_info(VkSurfaceKHR surface)
{
    // create swap-chain info
    return ds::swap_chain_info()
        .add_image_usage(ds::image_usage::color_attachment |
                         ds::image_usage::storage)
        .set_sync       (ds::sync::triple_buffering)
        .set_format     (ds::format::bgra8)
        .set_surface    (surface);
}

frame::frame(u8 index) :
    registry(res.gpu.add_registry("per-frame:" + std::to_string(index))),

    command_pool("pool", registry, ds::command_pool_info()
        .add_usage             (ds::command_pool_usage::transient  |
                                ds::command_pool_usage::reset)
        .set_queue_family_index(res.queue.family_index)),

    command("primary", registry, ds::command_info()
        .set_pool (command_pool)
        .set_level(ds::command_level::primary)
        .add_usage(ds::command_usage::one_time_submit)),

    resolution       ("resolution"     , registry, ubo_info().set_size(sizeof(std::pair<u32, u32>))),
    time             ("time"           , registry, ubo_info().set_size(sizeof(f32))),
    fence            ("frame-fence"    , registry, ds::fence_info()),
    acquire_semaphore("acquire-image"  , registry, ds::semaphore_info()),
    submit_semaphore ("submit"         , registry, ds::semaphore_info()),
    descriptors      (*this),
    gaussian         (registry),
    ray_marching     (registry),
    sobel            (registry) {}

per_swap::per_swap(u8 index) :
    index       (index),
    registry    (res.gpu.add_registry("per-swap:" + std::to_string(index))),
    primary_pass(*this),
    descriptors (*this) {}

resources::resources() :
    instance  (make_instance()),
    surface   (window.make_surface(instance.vk_instance)),
    gpu       (make_gpu(instance, surface)),
    registry  (gpu.add_registry("registry")),
    queue     ("graphics", registry, queue_info),
    swap_chain("swap-chain", registry, swap_chain_info(surface))
{
    const u8 num_swap_chain_images = swap_chain.images.size();

    frames  .reserve(config.num_frames_in_flight);
    per_swap.reserve(num_swap_chain_images);

    // Create per-frame registry
    for(u8 i = 0; i < config.num_frames_in_flight; ++i)
        frames.emplace_back(i);

    // Create per-swap-chain registry
    for(u8 i = 0; i < num_swap_chain_images; ++i)
        per_swap.emplace_back(i);
}

void
resources::reload_shaders()
{
    queue.wait_idle();

    ray_marching.reload_shaders();
}

void
resources::resize()
{
    gpu.wait_idle();
    queue.wait_idle();

    swap_chain.rebuild(swap_chain_info(surface));

    primary_pass.resize();
    ray_marching.resize();
    gaussian    .resize();

    for(dbl::per_swap &swap : per_swap)
        swap.resize();

    for(dbl::frame &frame : frames)
        frame.descriptors.update();
}

void
per_swap::resize()
{
    primary_pass.resize();
    descriptors .resize();
}

resources::~resources()
{
    gpu.wait_idle();
}

}

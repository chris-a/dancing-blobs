#pragma once

#include <ds/buffer>
#include <ds/command-pool>
#include <ds/fence>
#include <ds/instance>
#include <ds/numeric-types>
#include <ds/queue>
#include <ds/render-pass>
#include <ds/semaphore>
#include <ds/swap-chain>

#include "render/descriptors/descriptors.hxx"
#include "render/post-process/gaussian.hxx"
#include "render/post-process/sobel.hxx"
#include "render/ray-marching/ray-marching.hxx"
#include "render/render-pass/primary-render-pass.hxx"
#include "render/window/window.hxx"

#include <vector>

struct VkSurfaceKHR_T;
using VkSurfaceKHR = VkSurfaceKHR_T*;

namespace dbl {

class frame {
public:
    frame(u8 index);

    ds::registry &registry;

    // Command
    ds::command_pool command_pool;
    ds::command      command;

    // Buffers
    ds::buffer resolution;
    ds::buffer time;

    // Synchronization
    ds::fence     fence;
    ds::semaphore acquire_semaphore;
    ds::semaphore submit_semaphore;

    // Modules
    descriptors_frame  descriptors;
    gaussian_frame     gaussian;
    ray_marching_frame ray_marching;
    sobel_frame        sobel;
};

class per_swap {
public:
    per_swap(u8 index);
    void resize();

    u8 index;

    ds::registry &registry;

    primary_pass_per_swap primary_pass;
    descriptors_per_swap  descriptors;
};

class resources {
public:
    void resize();
    void reload_shaders();

    dbl::window window;

    // Core registry
    ds::instance    &instance;
    VkSurfaceKHR surface;
    ds::gpu         &gpu;
    ds::registry    &registry;
    ds::queue       queue;
    ds::swap_chain  swap_chain;

    // Render-passes
    dbl::primary_pass_global primary_pass;

    // Modules
    descriptors_global  descriptors;
    ray_marching_global ray_marching;
    sobel_global        sobel;
    gaussian_global     gaussian;

    std::vector<dbl::frame   > frames;   // per-frame registry
    std::vector<dbl::per_swap> per_swap; // per-swap-chain image registry

    resources();
    ~resources();
};

extern dbl::resources res;

}

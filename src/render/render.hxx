#pragma once

#include <ds/numeric-types>
#include <ds/barrier>

namespace ds {
    class command;
    class image;
}

namespace dbl {

class frame;
class per_swap;

class render {
public:
    static render &inst();

    u16 width () const;
    u16 height() const;

    bool ready() const;
    void update();
    void execute();

    // Command buffer recording
    void begin();
    void end();
    void ray_march();
    void post_process();

private:
    render() {}

    void resize();
    u32 acquire_swap_chain_image(u32 tries);

    void transition_vertical_to_general();
    void transition_swap_chain_to_general();
    void transition_swap_chain_to_present();

    void gaussian();
    void sobel();

    ds::command   &command();
    dbl::frame    &frame();
    dbl::per_swap &per_swap_chain();

private:
    bool first_run      = true;
    u8 frame_index      = 0;
    u8 swap_chain_index = 0;
};

}

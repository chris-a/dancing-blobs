#include "render/post-process/sobel.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

namespace dbl {

sobel_global::sobel_global() :
    compute_shader("sobel", res.registry, ds::shader_info()
            .set_stage(ds::shader_stage::compute)
            .set_file_path(shader_path)),

    pipeline_layout("sobel", res.registry, ds::pipeline_layout_info()
        .add_descriptor_layout(res.descriptors.post_process_layout)),

    pipeline("sobel", res.registry, ds::compute_pipeline_info()
        .set_shaders(compute_shader)
        .set_layout (pipeline_layout)) {}

sobel_frame::sobel_frame(ds::registry &registry) :
    timestamps("sobel:timestamp", registry, ds::query_pool_info()
        .set_size(2) // begin + end
        .set_type(ds::query_pool_type::timestamp)) {}

}

#include "render/post-process/gaussian.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

namespace dbl {

static ds::image_info
compute_image_info()
{
    return ds::image_info()
        .set_width (render::inst().width())
        .set_height(render::inst().height())
        .set_format(ds::format::bgra8)
        .set_aspect(ds::image_aspect::color)
        .set_msaa  (ds::msaa::x1)
        .set_usage (ds::image_usage::storage);
}

gaussian_global::gaussian_global() :
    compute_shader("gaussian", res.registry, ds::shader_info()
            .set_stage(ds::shader_stage::compute)
            .set_file_path(shader_path)),

    pipeline_layout("gaussian", res.registry, ds::pipeline_layout_info()
        .add_descriptor_layout(res.descriptors.post_process_layout)
        // is_vertical and use_separable
        .add_push_constant(ds::push_constant()
            .set_size(sizeof(std::pair<u32, u32>))
            .set_offset(0)
            .set_shader_stage(ds::shader_stage::compute))),

    pipeline("gaussian", res.registry, ds::compute_pipeline_info()
        .set_shaders(compute_shader)
        .set_layout (pipeline_layout)),

    horizontal("horizontal", res.registry, compute_image_info()),
    vertical  ("vertical"  , res.registry, compute_image_info()) {}

void
gaussian_global::resize()
{
    vertical  .rebuild(compute_image_info());
    horizontal.rebuild(compute_image_info());
}

gaussian_frame::gaussian_frame(ds::registry &registry) :
    timestamps("gaussian:timestamp", registry, ds::query_pool_info()
        .set_size(2) // begin + end
        .set_type(ds::query_pool_type::timestamp)) {}

}

#pragma once

#include <ds/compute-pipeline>
#include <ds/image>
#include <ds/pipeline-layout>
#include <ds/query-pool>
#include <ds/shader>

namespace dbl {

class gaussian_global {
public:
    gaussian_global();
    void resize();

    ds::shader           compute_shader;
    ds::pipeline_layout  pipeline_layout;
    ds::compute_pipeline pipeline;

    // Hold the horizontal result of the first post process
    ds::image horizontal;
    // Hold the vertical result of the second post process, if sobel enabled
    ds::image vertical;

    static constexpr const char *shader_path = "shaders/post-process/gaussian.comp.spv";
};

class gaussian_frame {
public:
    gaussian_frame(ds::registry &registry);

    ds::query_pool timestamps;
};

}

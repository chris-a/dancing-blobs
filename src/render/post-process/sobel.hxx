#pragma once

#include <ds/compute-pipeline>
#include <ds/image>
#include <ds/pipeline-layout>
#include <ds/query-pool>
#include <ds/shader>

namespace dbl {

class sobel_global {
public:
    sobel_global();

    ds::shader           compute_shader;
    ds::pipeline_layout  pipeline_layout;
    ds::compute_pipeline pipeline;

    static constexpr const char *shader_path = "shaders/post-process/sobel.comp.spv";
};

class sobel_frame {
public:
    sobel_frame(ds::registry &registry);

    ds::query_pool timestamps;
};

}

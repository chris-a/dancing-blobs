#pragma once

#include <ds/graphics-pipeline>
#include <ds/numeric-types>
#include <ds/pipeline-layout>
#include <ds/query-pool>
#include <ds/shader>

namespace ds {
    class registry;
}

namespace dbl {

// TODO: Once C++20 rolled out, use ds::shader_info (currently can't because std::string)
struct shader_info {
    u32 stage;
    const char *file_path;
};

template<typename T>
class graphics_pipeline {
public:
    graphics_pipeline();

    void resize();
    void reload_shaders();

private:
    void load_shaders();

    std::vector<ds::shader> shaders;
    ds::pipeline_layout     pipeline_layout;

public:
    ds::graphics_pipeline pipeline;
};

}


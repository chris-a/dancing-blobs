#include "config/config.hxx"
#include "render/ray-marching/ray-marching.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"
#include "msg/msg.hxx"

namespace dbl {

std::string
stage_to_str(u32 stage)
{
    switch(stage)
    {
        case ds::shader_stage::compute               : return "compute";
        case ds::shader_stage::fragment              : return "fragment";
        case ds::shader_stage::geometry              : return "geometry";
        case ds::shader_stage::tesellation_control   : return "tesellation-control";
        case ds::shader_stage::tesellation_evaluation: return "tesellation-evaluation";
        case ds::shader_stage::vertex                : return "vertex";
        default: msg_fatal("Unknown stage :'" + std::to_string(stage) + "'");
    }

    return "";
}

template <typename T>
graphics_pipeline<T>::graphics_pipeline() :
    pipeline_layout(T::name, res.registry, T::pipeline_layout_info()),

    pipeline((load_shaders(), // Load shaders before pipeline
              ds::graphics_pipeline(T::name, res.registry, T::pipeline_info(pipeline_layout, shaders)))) {}

template <typename T>
void
graphics_pipeline<T>::load_shaders()
{
    shaders.clear();
    shaders.reserve(T::shaders.size());

    for(const dbl::shader_info &info : T::shaders)
    {
        // TODO: constexpr
        std::string &&shader_name = std::string(T::name) + ":" + stage_to_str(info.stage);

        shaders.emplace_back(shader_name.data(), res.registry, ds::shader_info()
            .set_stage(info.stage)
            .set_file_path(info.file_path));
    }
}

template <typename T>
void
graphics_pipeline<T>::resize()
{
    pipeline.rebuild(T::pipeline_info(pipeline_layout, shaders));
}

template <typename T>
void
graphics_pipeline<T>::reload_shaders()
{
    load_shaders();
    pipeline.rebuild(T::pipeline_info(pipeline_layout, shaders));
}

template class graphics_pipeline<ray_marching_pipeline>;

}

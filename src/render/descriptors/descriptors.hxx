#pragma once

#include <ds/descriptor-layout>
#include <ds/descriptor-pool>
#include <ds/descriptor-set>

namespace ds {
    class registry;
}

namespace dbl {

class frame;
class per_swap;

class descriptors_global {
public:
    descriptors_global();

    ds::descriptor_layout ray_march_layout;
    ds::descriptor_layout post_process_layout;
    ds::descriptor_pool   pool;
    ds::descriptor_pool   post_process_pool;
};

class descriptors_frame {
public:
    descriptors_frame(dbl::frame&);

    void update_ray_march();
    void update();

private:
    dbl::frame &frame;

public:
    ds::descriptor_set ray_march_set;
};

class descriptors_per_swap {
public:
    descriptors_per_swap(dbl::per_swap&);

    void resize();
    void update_post_process();

private:
    dbl::per_swap &per_swap;

public:
    ds::descriptor_set gaussian_h_set;
    ds::descriptor_set gaussian_v_set;
    ds::descriptor_set sobel_set;
};

}

#include "config/config.hxx"
#include "render/descriptors/descriptors.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

#include <ds/descriptor-layout>
#include <ds/descriptor-pool>
#include <ds/descriptor-set>
#include <ds/image>

namespace dbl {

descriptors_global::descriptors_global() :
    ray_march_layout("ray-march", res.registry, ds::descriptor_layout_info()
        .shader_stage(ds::shader_stage::fragment)
            .add_uniform_buffer  ("resolution", 0)
            .add_uniform_buffer  ("time"      , 1)),

    post_process_layout("post-process-layout", res.registry, ds::descriptor_layout_info()
        .shader_stage(ds::shader_stage::compute)
            .add_storage_image("input" , 0)
            .add_storage_image("output", 1)),

    pool("pool", res.registry, ds::descriptor_pool_info()
        .add_layout(ray_march_layout)
        .add_max_sets(config.num_frames_in_flight)),

    post_process_pool("post-process-pool", res.registry, ds::descriptor_pool_info()
        .add_layout(post_process_layout)
        .add_max_sets(3*res.swap_chain.images.size())) {}

descriptors_frame::descriptors_frame(dbl::frame &frame) :
    frame(frame),

    ray_march_set("ray-march", frame.registry, ds::descriptor_set_info()
        .set_pool  (res.descriptors.pool)
        .set_layout(res.descriptors.ray_march_layout))
{
    update();
}

void
descriptors_frame::update_ray_march()
{
    ray_march_set.set_buffer("resolution", frame.resolution); 
    ray_march_set.set_buffer("time"      , frame.time); 

    ray_march_set.update();
}

void
descriptors_frame::update()
{
    update_ray_march();
}

static ds::descriptor_set_info
post_process_set_info()
{
    return ds::descriptor_set_info()
        .set_pool  (res.descriptors.post_process_pool)
        .set_layout(res.descriptors.post_process_layout);
}

descriptors_per_swap::descriptors_per_swap(dbl::per_swap &per_swap) :
    per_swap(per_swap),
    gaussian_h_set("gaussian-h", per_swap.registry, post_process_set_info()),
    gaussian_v_set("gaussian-v", per_swap.registry, post_process_set_info()),
    sobel_set     ("sobel"     , per_swap.registry, post_process_set_info())
{
    update_post_process();
}

/* Setup post-process sets, particular input/output images
 *
 * In general, ray-marching -> gaussian-h -> gaussian-v -> sobel -> swap-chain
 * However, gaussian and sobel are optional */
void
descriptors_per_swap::update_post_process()
{
    ds::image &swap_chain_image = res.swap_chain.images[per_swap.index];

    gaussian_h_set.set_image("input" , res.primary_pass.color , ds::image_layout::general);
    gaussian_h_set.set_image("output", res.gaussian.horizontal, ds::image_layout::general);

    gaussian_v_set.set_image("input" , res.gaussian.horizontal, ds::image_layout::general);
    gaussian_v_set.set_image("output",
                             config.sobel_enabled ? res.gaussian.vertical :
                                                    swap_chain_image ,
                             ds::image_layout::general);

    sobel_set.set_image("input",
                        config.gaussian_enabled ? res.gaussian.horizontal :
                                                  res.primary_pass.color,
                        ds::image_layout::general);

    sobel_set.set_image("output", swap_chain_image, ds::image_layout::general);


    // if gaussian enabled, use it's output for sobel's input
    // If gaussian is enabled, use horizontal output only if we aren't using separable
    sobel_set.set_image("input",
                        config.gaussian_enabled ? (config.separable_gaussian ? res.gaussian.vertical :
                                                                               res.gaussian.horizontal) :
                                                   res.primary_pass.color,
                        ds::image_layout::general);

    gaussian_v_set.update();
    gaussian_h_set.update();
    sobel_set     .update();
}

void
descriptors_per_swap::resize()
{
    update_post_process();
}

}

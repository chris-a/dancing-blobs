#pragma once

#include <ds/graphics-pipeline>
#include <ds/numeric-types>
#include <ds/pipeline-layout>
#include <ds/query-pool>

#include "config/config.hxx"
#include "render/graphics-pipeline/graphics-pipeline.hxx"

#include <array>

namespace ds {
    class registry;
}

namespace dbl {

struct ray_marching_pipeline {
    static constexpr ds::msaa    msaa         = ds::msaa::x1;
    static constexpr const char *name         = "ray-marching";
    static constexpr const char *subpass_name = "ray-marching";

    static constexpr std::array<dbl::shader_info, 2> shaders{{
        {ds::shader_stage::vertex  , "shaders/ray-marching/ray-marching.vert.spv"},
        {ds::shader_stage::fragment, "shaders/ray-marching/ray-marching.frag.spv"}
    }};

    static ds::pipeline_layout_info   pipeline_layout_info();
    static ds::graphics_pipeline_info pipeline_info(const ds::pipeline_layout     &layout,
                                                    const std::vector<ds::shader> &shaders);
};

class ray_marching_global : public graphics_pipeline<ray_marching_pipeline> {
};

class ray_marching_frame {
public:
    ray_marching_frame(ds::registry &registry);

    ds::query_pool timestamps;
};

}


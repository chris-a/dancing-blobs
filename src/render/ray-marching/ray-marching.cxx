#include "config/config.hxx"
#include "render/ray-marching/ray-marching.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

namespace dbl {

ds::graphics_pipeline_info
ray_marching_pipeline::pipeline_info(const ds::pipeline_layout     &layout,
                                   const std::vector<ds::shader> &shaders)
{
    return ds::graphics_pipeline_info()
        .set_msaa       (msaa)
        .set_shaders    (shaders)
        .set_subpass    (subpass_name)
        .set_render_pass(res.primary_pass.render_pass)
        .add_viewport(ds::viewport()
                      .set_width (render::inst().width())
                      .set_height(render::inst().height()))
        .set_layout(layout);
}

ds::pipeline_layout_info
ray_marching_pipeline::pipeline_layout_info()
{
    return ds::pipeline_layout_info()
        .add_descriptor_layout(res.descriptors.ray_march_layout)
        // is_vertical and use_separable
        .add_push_constant(ds::push_constant()
            .set_size(sizeof(f32))
            .set_offset(0)
            .set_shader_stage(ds::shader_stage::fragment));
}

ray_marching_frame::ray_marching_frame(ds::registry &registry) :
    timestamps("ray-marching:timestamp", registry, ds::query_pool_info()
        .set_size(2) // begin + end
        .set_type(ds::query_pool_type::timestamp)) {}

}

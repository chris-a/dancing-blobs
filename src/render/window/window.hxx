#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <ds/numeric-types>

#include <vector>

struct VkInstance_T;
using VkInstance = VkInstance_T*;

namespace dbl {

class window {
public:
    GLFWwindow *glfw_window;

    window();
    ~window();

    void close();
    void create();
    void destroy();
    bool should_close();

    VkSurfaceKHR make_surface(VkInstance instance);

    static std::vector<const char*> extensions();

    static double get_time();
};


}

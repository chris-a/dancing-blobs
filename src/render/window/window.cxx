#include <vulkan/vulkan.h>

#include "config/config.hxx"
#include "msg/msg.hxx"
#include "render/window/window.hxx"

namespace dbl {

window::window()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    glfw_window = glfwCreateWindow(config.window_default_width,
                                   config.window_default_height,
                                   "Dancing Blobs",
                                   nullptr,
                                   nullptr);

    glfwSetWindowUserPointer(glfw_window, this);
}

std::vector<const char*>
window::extensions()
{
    u32 glfw_extensions_count = 0;

    const char **glfw_extensions;

    glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extensions_count);

    if (!glfw_extensions_count)
        msg_fatal("GLFW asked for no extensions");

    std::vector<const char*> extensions(glfw_extensions, glfw_extensions + glfw_extensions_count);

    return extensions;
}

double window::get_time() { return glfwGetTime(); }

void window::close(void)
{
    glfwSetWindowShouldClose(glfw_window, true);
}

window::~window()
{
    glfwDestroyWindow(glfw_window);
    glfwTerminate();
}

bool window::should_close(void) 
{
    return glfwWindowShouldClose(glfw_window);
}

VkSurfaceKHR window::make_surface(VkInstance instance)
{
    VkSurfaceKHR surface;

    // Surface set to VK_NULL_HANDLE on error
    if (glfwCreateWindowSurface(instance, glfw_window, nullptr, &surface) != VK_SUCCESS)
        msg_fatal("Failed to create window surface");

    return surface;
}

}

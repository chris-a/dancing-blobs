#pragma once

#include <ds/framebuffer>
#include <ds/render-pass>

namespace ds {
    class image;
}

namespace dbl {

class per_swap;

class primary_pass_global {
public:
    primary_pass_global();
    void resize();

    ds::render_pass render_pass;
    ds::image color;
private:
    void add_resizable_resources();
};

class primary_pass_per_swap {
private:
    dbl::per_swap &per_swap;
public:
    primary_pass_per_swap(dbl::per_swap&);
    ds::framebuffer framebuffer;

    void resize();
};

}

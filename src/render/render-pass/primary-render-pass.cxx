#include <ds/framebuffer>
#include <ds/image>
#include <ds/render-pass>
#include <ds/registry>
#include <ds/swap-chain>

#include "config/config.hxx"
#include "render/render-pass/primary-render-pass.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"

namespace dbl {

static ds::image_info
attachment_info()
{ 
    return ds::image_info()
        .set_msaa  (ds::msaa::x1)
        .set_width (render::inst().width())
        .set_height(render::inst().height());
}

static ds::image_info
color_image_info()
{
    return attachment_info()
        .set_format(ds::format::bgra8)
        .set_aspect(ds::image_aspect::color)
        .set_usage (ds::image_usage::color_attachment |
                    ds::image_usage::storage);
}

static ds::attachments
attachments()
{
    return ds::attachments()
        .attachment("ray-march-output")
            .set_store (true)
            .set_clear (true)
            .set_format(ds::format::bgra8)
            .set_msaa  (ds::msaa::x1)
            .transition(ds::image_layout::undefined,
            /* If post-process enabled, transition image to general for usage in compute
               Otherwise, transiton the image (swap-chain) to present */
                        config.post_process_enabled() ? ds::image_layout::general :
                                                        ds::image_layout::present);
}

// Render-pass
static ds::render_pass_info
render_pass_info()
{
    return ds::render_pass_info()
        .set_attachments(attachments())
        // external -> mesh
        .add_subpass_dependency(ds::subpass_dependency()
            // src
            .set_src_subpass    ("external")
            .add_src_access_mask(0)
            .add_src_stage_mask (ds::pipeline_stage::color_attachment_output)
            // dst
            .set_dst_subpass    ("ray-marching")
            .add_dst_access_mask(ds::access_mask::color_attachment_write)
            .add_dst_stage_mask (ds::pipeline_stage::color_attachment_output))
        .subpass("ray-marching")
            .color("ray-march-output");
}

static ds::framebuffer_info
framebuffer_info(u8 index)
{
    auto info = ds::framebuffer_info()
        .set_width      (render::inst().width ())
        .set_height     (render::inst().height())
        .set_render_pass(res.primary_pass.render_pass);

    // Output to color image for post processing if enabled
    if(config.post_process_enabled())
        return info.add_image("ray-march-output", res.primary_pass.color);
    // Otherwise output directly to the swap-chain
    else
        return info.add_image("ray-march-output", res.swap_chain.images[index]);
}

primary_pass_global::primary_pass_global() :
    render_pass("primary"      , res.registry, render_pass_info()),
    color      ("primary:color", res.registry, color_image_info()) {}

void
primary_pass_global::resize()
{
    color.rebuild(color_image_info());
}

primary_pass_per_swap::primary_pass_per_swap(dbl::per_swap &per_swap) :
    per_swap(per_swap),
    framebuffer("primary", per_swap.registry, framebuffer_info(per_swap.index)) {}

void
primary_pass_per_swap::resize()
{
    framebuffer.rebuild(framebuffer_info(per_swap.index));
}

}

#include <ds/buffer>
#include <ds/command-pool>
#include <ds/command>
#include <ds/fence>
#include <ds/framebuffer>
#include <ds/gpu>
#include <ds/image>
#include <ds/instance>
#include <ds/pipeline-layout>
#include <ds/query-pool>
#include <ds/queue>
#include <ds/semaphore>
#include <ds/swap-chain>

#include <chrono>
#include <thread>
#include <cmath>

#include "config/config.hxx"
#include "msg/msg.hxx"
#include "render/descriptors/descriptors.hxx"
#include "render/ray-marching/ray-marching.hxx"
#include "render/render.hxx"
#include "render/resources/resources.hxx"
#include "render/window/window.hxx"

#include <filesystem>

namespace fs = std::filesystem;

namespace dbl {

static f32
regulate_fps(f32 target_fps)
{
    namespace chrono = std::chrono;

    static const f64 period = 1.0 / target_fps;
    i32 sleep_time_us = i32(period * 1e6);
    i32 render_time_us;

    static auto last_time    = chrono::high_resolution_clock::now();
           auto current_time = chrono::high_resolution_clock::now();

    render_time_us = chrono::duration_cast<chrono::microseconds>(current_time - last_time).count();
    sleep_time_us -= render_time_us;

    if(sleep_time_us > 0)
        std::this_thread::sleep_for(chrono::microseconds(sleep_time_us));

    last_time = chrono::high_resolution_clock::now();

    // updated in units of seconds
    f32 delta_time = static_cast<f32>(sleep_time_us)/1e6;

    if(delta_time < 0)
        delta_time *= -1;

    return delta_time;
}

static ds::pipeline_barrier
compute_to_compute_barrier(const ds::image &image)
{
    return ds::pipeline_barrier()
        .add_src_stage(ds::pipeline_stage::compute_shader)
        .add_dst_stage(ds::pipeline_stage::compute_shader)
        .add_image_barrier(ds::image_barrier()
            .set_image     (image)
            .add_src_mask  (ds::access_mask::shader_write)
            .add_dst_mask  (ds::access_mask::shader_read)
            .set_old_layout(ds::image_layout::general)
            .set_new_layout(ds::image_layout::general));
}

ds::command  &render::command() { return frame().command; }
dbl::frame    &render::frame()           { return res.frames  [frame_index     ]; }
dbl::per_swap &render::per_swap_chain()  { return res.per_swap[swap_chain_index]; }
bool          render::ready () const    { return !res.window.should_close(); }
u16           render::width () const    { return res.swap_chain.width ; }
u16           render::height() const    { return res.swap_chain.height; }
void          render::end   ()          { command().end  (); }

void render::begin()
{
    command().begin(); 

    if(config.post_process_enabled())
    {
        if(first_run)
        {
            transition_vertical_to_general();
            first_run = false;
        }

        transition_swap_chain_to_general();
    }
}

void
render::transition_vertical_to_general()
{
    command()
        .pipeline_barrier(ds::pipeline_barrier()
        .add_src_stage(ds::pipeline_stage::top_of_pipe)
        .add_dst_stage(ds::pipeline_stage::compute_shader)
        .add_image_barrier(ds::image_barrier()
            .set_image     (res.gaussian.vertical)
            .set_old_layout(ds::image_layout::undefined)
            .set_new_layout(ds::image_layout::general)
            .add_src_mask  (0)
            .add_dst_mask  (ds::access_mask::shader_read)));

    command()
        .pipeline_barrier(ds::pipeline_barrier()
        .add_src_stage(ds::pipeline_stage::top_of_pipe)
        .add_dst_stage(ds::pipeline_stage::compute_shader)
        .add_image_barrier(ds::image_barrier()
            .set_image     (res.gaussian.horizontal)
            .set_old_layout(ds::image_layout::undefined)
            .set_new_layout(ds::image_layout::general)
            .add_src_mask  (0)
            .add_dst_mask  (ds::access_mask::shader_read)));
}

void
render::transition_swap_chain_to_general()
{
    command()
        // Transiton swap chain image from undefined -> general for compute
        .pipeline_barrier(ds::pipeline_barrier()
            .add_src_stage(ds::pipeline_stage::top_of_pipe)
            .add_dst_stage(ds::pipeline_stage::compute_shader)
            .add_image_barrier(ds::image_barrier()
                .set_image     (res.swap_chain.images[swap_chain_index])
                .set_old_layout(ds::image_layout::undefined)
                .set_new_layout(ds::image_layout::general)
                .add_src_mask  (0)
                .add_dst_mask  (ds::access_mask::shader_write)));
}

void
render::transition_swap_chain_to_present()
{
    // Transiton swap chain image from general -> present for presentation 
    command()
            .pipeline_barrier(ds::pipeline_barrier()
            .add_src_stage(ds::pipeline_stage::compute_shader)
             .add_dst_stage(ds::pipeline_stage::bottom_of_pipe)
             .add_image_barrier(ds::image_barrier()
                 .set_image     (res.swap_chain.images[swap_chain_index])
                 .set_old_layout(ds::image_layout::general)
                 .set_new_layout(ds::image_layout::present)
                 .add_src_mask  (ds::access_mask::shader_write)
                 .add_dst_mask  (ds::access_mask::memory_read)));
}

void
render::ray_march()
{
    ds::framebuffer &framebuffer = per_swap_chain().primary_pass.framebuffer;

    auto render_pass_cmd = ds::render_pass_command()
        .set_width      (width())
        .set_height     (height())
        .set_render_pass(res.primary_pass.render_pass)
        .set_framebuffer(framebuffer);

    command()
        .reset_query_pool(frame().ray_marching.timestamps)
        .reset_query_pool(frame().sobel.timestamps)
        .reset_query_pool(frame().gaussian.timestamps)
        .write_timestamp(ds::pipeline_stage::top_of_pipe, frame().ray_marching.timestamps, 0)
        .render_pass(render_pass_cmd)
        .pipeline(res.ray_marching.pipeline)
            .push_constant<f32>(ds::shader_stage::fragment, config.blob_inc)
            .descriptor_set(frame().descriptors.ray_march_set);

    vkCmdDraw(command().vk, 3, 1, 0, 0);

    command()
        .end_render_pass()
        .write_timestamp(ds::pipeline_stage::compute_shader, frame().ray_marching.timestamps, 1);
}

void
render::gaussian()
{
    struct push_const_data {
        u32 vertical;
        u32 use_separable;
    };

    push_const_data push_const = {
        .vertical      = 0,
        .use_separable = config.separable_gaussian
    };

    command()
        .write_timestamp(ds::pipeline_stage::compute_shader, frame().gaussian.timestamps, 0)
        .pipeline(res.gaussian.pipeline)
            // Horizontal gaussian (first-pass)
            .descriptor_set(per_swap_chain().descriptors.gaussian_h_set)
            .push_constant(ds::shader_stage::compute, push_const)
            .dispatch(std::ceil(width()/32.0), std::ceil(height()/32.0));

    if(config.separable_gaussian)
    {
        push_const.vertical = 1;
        // Vertical gaussian (second-pass)
        command()
                .pipeline_barrier(compute_to_compute_barrier(res.gaussian.vertical))
                .push_constant(ds::shader_stage::compute, push_const)
                .descriptor_set(per_swap_chain().descriptors.gaussian_v_set)
                .dispatch(std::ceil(width()/32.0), std::ceil(height()/32.0));
    }
    command().write_timestamp(ds::pipeline_stage::bottom_of_pipe, frame().gaussian.timestamps, 1);
}

void
render::sobel()
{
    command()
        .write_timestamp(ds::pipeline_stage::compute_shader, frame().sobel.timestamps, 0)
        .pipeline(res.sobel.pipeline)
            .descriptor_set(per_swap_chain().descriptors.sobel_set)
            .dispatch(std::ceil(width()/32.0), std::ceil(height()/32.0))
        .write_timestamp(ds::pipeline_stage::bottom_of_pipe, frame().sobel.timestamps, 1);
}

void
render::post_process()
{
    if(config.gaussian_enabled)
        gaussian();

    if(config.post_process_enabled())
        command().pipeline_barrier(compute_to_compute_barrier(res.gaussian.horizontal));

    if(config.sobel_enabled)
        sobel();

    if(config.post_process_enabled())
        transition_swap_chain_to_present();
}

void
print_gpu_time(std::string name, const ds::query_pool &pool)
{
    ds::query_pool_get get {
        .first_index = 0,
        .count       = 2
    };

    std::vector<u64> timestamps = pool.get_timestamps<u64>(get);

    u64 nanosecond_difference = timestamps[1] - timestamps[0];

    msg_info(name + " - " + std::to_string(nanosecond_difference / 1e6) + "ms", "");
}

u32
render::acquire_swap_chain_image(u32 tries = 0)
{
    auto [swap_chain_index, valid] = res.swap_chain.acquire_next_image(frame().acquire_semaphore);

    if(tries == 3)
        msg_fatal("Failed to acquire swap chain image");

    if(!valid)
    {
        resize();
        acquire_swap_chain_image(tries+1);
    }

    return swap_chain_index;
}

void
render::update()
{
    swap_chain_index = acquire_swap_chain_image();

    frame().fence.wait();
    frame().fence.reset();

    print_gpu_time("primary-render-pass", frame().ray_marching.timestamps);
    msg_info(" -- ", "");
    print_gpu_time("gaussian", frame().gaussian.timestamps);                   
    msg_info(" -- ", "");
    print_gpu_time("sobel", frame().sobel.timestamps);                   
    msg_info("\r", "");

    const f32 time = window::get_time();

    std::pair<u32, u32> dimensions{width(), height()};

    // Update uniform buffers
    frame().time      .write(&time      , sizeof(f32));
    frame().resolution.write(&dimensions, sizeof(std::pair<u32, u32>));
}

void
render::execute(void)
{
    auto submit_info = ds::queue_submit_info()
        .add_command         (command().vk)
        .add_wait_semaphore  (frame().acquire_semaphore, ds::pipeline_stage::color_attachment_output)
        .add_signal_semaphore(frame().submit_semaphore)
        .set_signal_fence    (frame().fence);

    auto present_info = ds::queue_present_info()
        .add_swap_chain     (res.swap_chain, swap_chain_index   )
        .add_wait_semaphore(frame().submit_semaphore);

    res.queue.submit (submit_info );
    std::vector<VkResult> present_results = res.queue.present(present_info);

    if(present_results[0] == VK_SUBOPTIMAL_KHR || present_results[0] == VK_ERROR_OUT_OF_DATE_KHR)
        resize();

    frame_index = (frame_index+1)%config.num_frames_in_flight;

    regulate_fps(config.target_fps);
}

void
render::resize()
{
    first_run = true;
    res.resize();
}

render&
render::inst()
{
    static dbl::render render;

    return render;
}

}

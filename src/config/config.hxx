#pragma once

#include <ds/numeric-types>

namespace dbl {

class configuration {
public:
    bool post_process_enabled() const { return sobel_enabled || gaussian_enabled; }

    /* Ideally between [0.5, 2.5]
     * Less blobs at higher numbers. Much higher performance */
    static constexpr f32  blob_inc              = 0.9;

    static constexpr bool sobel_enabled         = true;
    static constexpr bool separable_gaussian    = false;
    static constexpr bool gaussian_enabled      = true;
    static constexpr f32  target_fps            = 60.0; 
    static constexpr u8   num_frames_in_flight  = 2;
    static constexpr u32  window_default_width  = 1290;
    static constexpr u32  window_default_height = 730;
};

extern configuration config;

}

#include "msg/msg.hxx"
#include "render/render.hxx"

int
main(void)
{
    dbl::render &render = dbl::render::inst();

    while(render.ready())
    {
        render.update();

        // Record command buffers
        render.begin();
            render.ray_march();
            render.post_process();
        render.end();

        // Submit command buffer
        render.execute();
    }

    return 0;
}

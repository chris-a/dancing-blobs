#include <string>

#include "msg.hxx"

void msg(msg_level                                level,
         const std::string                        &log_msg,
         const std::string                        line_end,
         const std::experimental::source_location &location)
{
    const char *type = nullptr;

    auto print = [](const std::string &s, FILE *stream)
    {
        std::fwrite(s.data(), s.size(), 1, stream);
    };

    switch(level) {
        case msg_level::info :
        {
            print(log_msg + line_end , stdout);

            break;
        }
        case msg_level::warn :
        {
            type = "[warn]: ";
        }
        case msg_level::fatal:
        {
            if(!type)
                type = "[fatal]: ";

            std::string &&err =
                colors::bold_red + std::string(type) + colors::normal      +
                colors::red      + log_msg  + "\n"                         +
                colors::yellow   + location.file_name()                    +
                colors::yellow   + ":" + std::to_string( location.line() ) + " in " +
                colors::yellow   + location.function_name()                + "()"   +
                colors::normal   + line_end;

            print(err, stderr);
            exit(-1);
        }
        case msg_level::vulkan_validation:
        {
            std::string &&err = colors::bold_red + std::string("\r[validation layer]: ") +
                                colors::normal +
                                colors::red + log_msg + line_end +
                                colors::normal;

            print(err, stderr);
        }
    }
}

void msg_info(const std::string                        &log_msg,
              const std::string                        line_end,
              const std::experimental::source_location &location)
{
    msg(msg_level::info, log_msg, line_end, location);
}


void msg_fatal(const std::string                        &log_msg,
               const std::string                        line_end,
               const std::experimental::source_location &location)
{
    msg(msg_level::fatal, log_msg, line_end, location);
}

void msg_warn(const std::string                        &log_msg,
              const std::string                        line_end,
              const std::experimental::source_location &location)
{
    msg(msg_level::warn, log_msg, line_end, location);
}


void msg_callback(msg_level                                   level,
                  const std::string                        &log_msg,
                  const std::experimental::source_location &location)
{
    msg(level, log_msg, "\n", location);
}

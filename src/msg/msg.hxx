#pragma once

#include <string>
#include <experimental/source_location>

#include <ds/log>

// terminal colors
namespace colors {
    constexpr const char *normal       = "\x1B[0m" ;
    constexpr const char *red          = "\x1B[31m";
    constexpr const char *green        = "\x1B[32m";
    constexpr const char *yellow       = "\x1B[33m";
    constexpr const char *blue         = "\x1B[34m";
    constexpr const char *magenta      = "\x1B[35m";
    constexpr const char *cyan         = "\x1B[36m";
    constexpr const char *white        = "\x1B[37m";
    constexpr const char *bold_normal  = "\033[1m" "\x1B[0m" ;
    constexpr const char *bold_red     = "\033[1m" "\x1B[31m";
    constexpr const char *bold_green   = "\033[1m" "\x1B[32m";
    constexpr const char *bold_yellow  = "\033[1m" "\x1B[33m";
    constexpr const char *bold_blue    = "\033[1m" "\x1B[34m";
    constexpr const char *bold_magenta = "\033[1m" "\x1B[35m";
    constexpr const char *bold_cyan    = "\033[1m" "\x1B[36m";
    constexpr const char *bold_white   = "\033[1m" "\x1B[37m";
}

using msg_level = ds::log;

void msg(msg_level                                level,
         const std::string                        &log_msg,
         const std::string                        line_end = "\n",
         const std::experimental::source_location &location =
               std::experimental::source_location::current());

void msg_info(const std::string                        &log_msg,
              const std::string                        line_end = "\n",
              const std::experimental::source_location &location =
                    std::experimental::source_location::current());

void msg_fatal(const std::string                        &log_msg,
               const std::string                        line_end = "\n",
               const std::experimental::source_location &location =
                     std::experimental::source_location::current());

void msg_warn(const std::string                        &log_msg,
              const std::string                        line_end = "\n",
              const std::experimental::source_location &location =
                    std::experimental::source_location::current());

void msg_callback(msg_level                                level,
                  const std::string                        &log_msg,
                  const std::experimental::source_location &location =
                  std::experimental::source_location::current());

# Dancing Blobs

Dancing blobs is a real-time ray-marched scene with optional gaussian blur and sobel edge detection post-processing effects.

Vulkan is leveraged to for both rendering the ray-marched scene and performing the post-processing in compute shaders.

![Dancing Blobs with no post-processing](https://s1.webmshare.com/WxdqK.webm)

## Requirements
* Vulkan
* c++2a compatible compiler
* glfw3
* [Dream Shader](https://gitlab.com/chris-a/dream-shader)


## Build
Clone dancing-blobs and dream-shader submodule
```
git clone --recursive https://gitlab.com/chris-a/dancing-blobs
meson build
ninja -C build
```

Run the produced executable
```
./build/dancing-blobs
```

## The scene has adjustable variables in the [configuration file](src/config/config.hxx)

| Relevant variables   | Default |
| ---                  |  ------ |
| sobel_enbled         | true    |
| gaussian_enabled     | true    |
| separable_gaussian   | true    |
| blob_inc             | 0.7     |

## The ray marching [fragment shader](shaders/ray-marching/ray-marching.frag)

The blobs in the scene are produced by tracing sphere SDFs with a distortion applied.

A rotation is then applied to each blob and a smooth union operation allows them to meld together.

Finally, we compute the normals by sampling points for the gradient and use that to color the blobs.

We can modify the number of blobs with the **config.blob_inc** value.

## The gaussian [compute shader](shaders/post-process/gaussian.comp)
![Gaussian blur](https://s1.webmshare.com/6VPeo.webm)

There are two options for performing the guassian blur: normal convolution, separable convolution.

A 5x5 gaussian kernel, with sigma = 1.0, is used to convolve the original image.

The gaussian post-process can enabled/disabled by toggling **config.gaussian_enable**.
Likewise, we can switch between the normal and separable convolution with **config.gaussian_separable**.

The separable convolution is performed by invoking the compute shader twice. First horizontally and then vertically on the image.
Separable convolution benefits us by producing the same image with far less multiplications.

Performance comparison in quick test at 1920x1080 on a GTX 1050:

| Technique            | GPU Time|
| -------------------- | ------- |
| Normal convolution   | 3.66ms  |
| Separable convolution| 2.62ms  |

## The sobel [compute shader](shaders/post-process/sobel.comp)
![Sobel operator after gaussian](https://s1.webmshare.com/jvJxA.webm)

The sobel operator is the final post process in the scene.

To perform this operation, the two sobel kernels are convolved with a grayscale of the scene.

The magnitude of the result is then used for the final output pixel color.
